# Leximots

## Description

## Utilisation

## Contribution

### Organisation du code

La logique du jeu 

Les fontes disponibles pour le jeu sont gérées par [`fontmanager.js`](fontmanager.js).
Il définit un objet `FontManager` qui contient toutes les fonctions utiles pour changer la fonte du jeu.

### Je veux modifier Leximots !

Pour contribuer de manière substantielle à Leximots, vous devez connaître les rudiments des langages HTML, CSS et JavaScript.
Toutefois, un certain nombre de modifications ne nécessite **aucune** connaissance dans ces langages.
Nous avons simplifié et documenté ces dernières pour vous permettre de facilement adapter Leximots à vos besoins :
* [Je veux ajouter une fonte](#ajouter-une-fonte)
* [Je veux ajouter ou retirer des mots](#ajouter-ou-retirer-des-mots)

#### Ajouter une fonte

#### Ajouter ou retirer des mots

## Attribution

Leximots utilise les ressources suivantes :
* tailwindcss v3.2.1 (MIT License) : https://tailwindcss.com
* canvas-confetti v1.6.0 (ISC License) : https://github.com/catdad/canvas-confetti