"use strict";

const listeLettres = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","à","â","é","è","ê","ë","î","ô","û"];

var motCourant = [];
const motReference = document.getElementById("mot-reference");
const zoneExercice = document.getElementById("zone-exercice");
var ctIdEtiquettes = 0;
var ctLettres = 0;
var tempsAffichage = 2000;

function alea(min,max) {
  const nb = min + (max - min + 1) * Math.random();
  return Math.floor(nb);
}

function creeListeLettres() {
  listeLettres.forEach(createLetter);
}

function createLetter(item) {
  const newTexte = document.createElement("span");
  const newDiv = document.createElement("div");
  newDiv.id = "lettre-" + item;
  newDiv.classList.add(
    "bg-blue-500",
    "hover:bg-blue-400",
    "flex",
    "justify-center",
    "items-center",
    "border-b-4",
    "border-blue-700",
    "hover:border-blue-500",
    "rounded",
    "cursor-grab",
    "h-24",
    "w-24"
  );
  newDiv.setAttribute("draggable", "true");
  newDiv.addEventListener("dragstart", drag);
  // Create the text
  newTexte.innerText = item;
  newTexte.classList.add(
    "fonte-lettre",
    "text-white",
    "text-5xl",
    "font-bold"
  );
  newDiv.appendChild(newTexte);
  document.getElementById("zone-lettres").appendChild(newDiv);
}

function creeListeMots() {
  listeMots.forEach(addWordToWordsList);
}

function addWordToWordsList(item) {
  const d = document.getElementById("liste-mots");
  const option = d.appendChild(document.createElement("option"));
  option.text = item;
}

function creeEtiquettes() {
  motCourant.forEach(createEtiquette);
}

function createEtiquette(item) {
  var newDiv = document.createElement("div");
  newDiv.id = "etiquette-" + item;
  newDiv.classList.add(
    "fonte-lettre",
    "h-24",
    "w-24",
    "flex",
    "bg-orange-300",
    "rounded",
    "border-b-4",
    "border-orange-600",
    "mr-8",
    "last:mr-0"
  );
  newDiv.addEventListener("drop", drop);
  newDiv.addEventListener("dragover", allowDrop);
  newDiv.addEventListener("contextmenu",function(e){e.preventDefault();});
  zoneExercice.appendChild(newDiv);
}

function videZoneExercice() {
  while (zoneExercice.firstChild) {
    zoneExercice.removeChild(zoneExercice.firstChild);
  }
  ctIdEtiquettes = 0;
  ctLettres = 0;
}

function efface(ev) {
  const objetCible = document.getElementById(ev.target.id);
  if (ev.button === 2) {
    objetCible.parentNode.removeChild(objetCible);
    ctLettres-= 1;
  }
}

function afficheMotChoisi() {
  const liste = document.getElementById("liste-mots");
  const mot = liste.options[liste.selectedIndex];
  setupWord(mot.value);
  mot.selected = false;
}

function cacheMot() {
  motReference.classList.remove("fonte-lettre");
  motReference.textContent = "X".repeat(motCourant.length);

  // Re-enable the "show again" button
  document.getElementById("btrevoir").disabled = false;
}

function afficheMotAleatoire() {
  setupWord(listeMots[alea(0,listeMots.length)]);
}

function voirMot() {
  // disable the "show again" button
  document.getElementById("btrevoir").disabled = true;

  motReference.classList.add("fonte-lettre");
  // join the array back into a string
  motReference.textContent = motCourant.join("");
  showTimerBar();
  setTimeout(cacheMot, tempsAffichage);
}

/**
 * Setups the exercise zone with the given word.
 * @param {String} word The word for this game.
 */
function setupWord(word) {
  const temps = sessionStorage.getItem("temps");
  if (temps && temps !== "undefined") {
    tempsAffichage = temps;
  }

  motCourant = word.split(""); // split the word into an array of characters
  toggleGameState(); // Show the game and hide the word-picker
  videZoneExercice();
  creeEtiquettes();
  voirMot();
}

function drag(ev) {
  var infosObjet = ev.target.id;
  ev.dataTransfer.setData("text", infosObjet);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  const idLettre = ev.dataTransfer.getData("text");
  var lettreCopy = document.getElementById(idLettre).cloneNode(true);
  ev.preventDefault();
  if (ev.target.lastChild !== null) return; // there's already a letter there, so quit

  lettreCopy.id = idLettre + "_" + ctIdEtiquettes;
  ctIdEtiquettes += 1;
  lettreCopy.addEventListener("mouseup", efface);
  ev.target.appendChild(lettreCopy);
  ctLettres += 1;
  if (ctLettres === motCourant.length) {
    setTimeout(verification, 250);
  }
}

function verification() {
  var motSaisi = motCourant.reduce((acc, curr) => {
    return acc.concat(document.getElementById("etiquette-" + curr).lastChild.textContent);
  }, "");
  if (motSaisi === motCourant.join("")) {
    toggleWinningDialog();

    // Show some confettis!
    var end = Date.now() + (5 * 1000);
    (function frame() {
      confetti({
        particleCount: 2,
        angle: 60,
        spread: 55,
        origin: { x: 0 }
      });
      confetti({
        particleCount: 2,
        angle: 120,
        spread: 55,
        origin: { x: 1 }
      });

      if (Date.now() < end) {
        requestAnimationFrame(frame);
      }
    }());
  }
  toggleGameState(); // Hide the game and show the word-picker
}

function toggleGameState() {
  document.getElementById("game").toggleAttribute("hidden");
  document.getElementById("pick-a-word").toggleAttribute("hidden");
}

function toggleOptions() {
  document.getElementById("ecran-blanc").toggleAttribute("hidden");
  document.getElementById("options").toggleAttribute("hidden");
}

function toggleWinningDialog() {
  document.getElementById("ecran-blanc").toggleAttribute("hidden");
  document.getElementById("win").toggleAttribute("hidden");
}

function valideOptions() {
  // Font
  const btFonte = [].slice.call(document.getElementsByName("fonte")); // have to use this to turn it into an array
  FontManager.setLetterFont(btFonte.find((bt) => bt.checked).value);

  // Time
  switch(document.getElementById("time-slider")) {
    case "1": // 1 second
      sessionStorage.setItem("temps",1000);
      break;
    case "2": // 2 seconds
      sessionStorage.setItem("temps",2000);
      break;
    case "3": // 3 seconds
      sessionStorage.setItem("temps",3000);
      break;
    case "4": // 4 seconds
      sessionStorage.setItem("temps",4000);
      break;
    case "5": // 5 seconds
      sessionStorage.setItem("temps",5000);
      break;
    case "6": // 10 seconds
      sessionStorage.setItem("temps",10000);
      break;
    case "7": // Keep shown
      sessionStorage.setItem("temps",360000);
      break;
  }

  toggleOptions(); // and we hide the options dialog
}

/**
 * Handles the timer bar.
 * The timer bar depicts the remaining time the word will be shown.
 */
function showTimerBar() {
  var timerContainer = document.getElementById("timer-container");
  var timerBar = document.getElementById("timer-bar");
  var width = 100;
  var intervalId = setInterval(updateTimer, tempsAffichage/100);

  // show the timer container
  timerContainer.classList.toggle("invisible");

  function updateTimer() {
    if (width <= 0) {
      clearInterval(intervalId);
      // hide the timer container again
      timerContainer.classList.toggle("invisible");
    } else {
      width = width - 1;
      timerBar.style.width = width + "%";
    }
  }
}

creeListeLettres();
creeListeMots();
FontManager.loadDefaultFont();
