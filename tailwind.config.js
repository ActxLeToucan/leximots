/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./leximots.js"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
